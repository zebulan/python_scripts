def _ccw_90(lst):
    """ Rotate a list Counter-Clockwise, 90 degrees"""
    return list(reversed(list(zip(*lst))))


def _cw_90(lst):
    """ Rotate a list Clockwise, 90 degrees"""
    return list(zip(*reversed(lst)))


def rotate_90(matrix, rotations=1, cw=True):
    """
    :param matrix: Two-Dimensional list.
    :param rotations: Integer of number of 90 degree rotations to perform.
    :param cw: True if clockwise, False if counter-clockwise.
    :return: List that has been rotated if rotations > 0.
             If rotations == 0, returns original list.
    """
    rotations = int(rotations) % 4  # force into a positive integer (0-3)
    if rotations == 0:              # rotation is unnecessary
        return matrix
    elif rotations == 1:            # single rotation
        return _cw_90(matrix) if cw else _ccw_90(matrix)
    elif rotations == 2:            # single rotation equal to two rotations
        return list(a[::-1] for a in reversed(matrix))
    elif rotations == 3:            # single rotation equal to three rotations
        return _ccw_90(matrix) if cw else _cw_90(matrix)


def rotate_45(lst, rotations=1, cw=True):
    return _cw_45(lst) if cw else _ccw_45(lst)


def _cw_45(lst):
    height = len(lst)
    width = max(len(a) for a in lst)
    # normalize_list() ?

    start = 0
    rotated = list()
    for a in range(0, height, height-1):
        for b in range(start, width):
            tmp = list()
            x, y = (b, a) if a == 0 else (a, b)
            for c in range(y, width):
                if x < 0 or x == height:
                    break
                tmp.append(lst[x][c])
                x -= 1
            if tmp:
                rotated.append(tmp)
        start += 1
    return rotated


def _ccw_45(lst):
    height = len(lst)
    width = max(len(a) for a in lst)

    start = width - 1
    rotated = list()
    for a in range(height):
        for b in range(start, -1, -1):
            start = start - 1 if start > 0 else 0
            tmp = list()
            for c in range(height):
                x = a + c
                y = b + c
                if x > height-1 or y > width-1:
                    break
                tmp.append(lst[x][y])
            rotated.append(tmp)
    return rotated


def normalize_list(lst):
    # height = len(lst)
    widths = [len(a) for a in lst]
    if len(set(widths)) == 1:
        return lst  # widths are all the same
    # what if the list already has none's in it?
    # it would return that list above... ?
    # should I just strip each None out in the cw/ccw functions?

    max_width = max(widths)
    add_left = True  # if cw else False ??
    final_list = list()
    for row in lst:
        length = len(row)
        if length == max_width:  # don't adjust these rows
            final_list.append(row)
            add_left = False     # if add_left else True ??
        else:
            if add_left:  # add None(s) to left of number(s) in row
                final_list.append(([None] * (max_width - length)) + row)
            else:         # add None(s) to right of number(s) in row
                final_list.append((row + ([None] * (max_width - length))))
    return final_list


print(rotate_45([[None, None, 1],
                 [None, 4, 2],
                 [7, 5, 3],
                 [8, 6, None],
                 [9, None, None]], cw=False))
print()
print(rotate_45([[None, None, 1],
                 [None, 4, 2],
                 [7, 5, 3],
                 [8, 6, None],
                 [9, None, None]]))
print()
# print(rotate_45([[1], [4, 2], [7, 5, 3], [8, 6], [9]]))
# print()
# print(rotate_45([[1], [4, 2], [7, 5, 3], [8, 6], [9]], cw=True))


# # print(normalize_list([[1, 2], [3, 4], [5, 6]]))
# print(normalize_list([[1], [4, 2], [7, 5, 3], [8, 6], [9]]))
# print(normalize_list([[4], [3, 8], [2, 7, 12], [1, 6, 11], [5, 10], [9]]))


# if __name__ == '__main__':
#     # print(rotate_45([[1, 2, 3],
#     #                  [4, 5, 6],
#     #                  [7, 8, 9]]))
#     #
#     # print(rotate_45([[1, 2, 3],
#     #                  [4, 5, 6],
#     #                  [7, 8, 9]], cw=False))
#     #
#     # print(rotate_45([[1, 2, 3, 4],
#     #                  [5, 6, 7, 8],
#     #                  [9, 10, 11, 12]]))
#     #
#     # print(rotate_45([[1, 2, 3, 4],
#     #                  [5, 6, 7, 8],
#     #                  [9, 10, 11, 12]], cw=False))
#
#     assert rotate_45([[1, 2, 3],
#                       [4, 5, 6],
#                       [7, 8, 9]]) == [[1], [4, 2], [7, 5, 3], [8, 6], [9]]
#
#     assert rotate_45([[1, 2, 3],
#                       [4, 5, 6],
#                       [7, 8, 9]], cw=False) == \
#            [[3], [2, 6], [1, 5, 9], [4, 8], [7]]
#
#
#     assert rotate_45([[1, 2, 3, 4],
#                       [5, 6, 7, 8],
#                       [9, 10, 11, 12]]) == \
#            [[1], [5, 2], [9, 6, 3], [10, 7, 4], [11, 8], [12]]
#
#     assert rotate_45([[1, 2, 3, 4],
#                       [5, 6, 7, 8],
#                       [9, 10, 11, 12]], cw=False) == \
#            [[4], [3, 8], [2, 7, 12], [1, 6, 11], [5, 10], [9]]
