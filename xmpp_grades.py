from bs4 import BeautifulSoup
from operator import itemgetter
from prettytable import PrettyTable
from requests import get


def g2s(grade):
    """ Return a score based on a given connection grade """
    score = {'A': 1, 'Aâ»': 2, 'B': 3, 'B-': 4, 'C': 5,
             'C-': 6, 'F': 7, 'F-': 8, 'T': 9, '?': 10}
    return score[grade] if grade in score else 10


def s2g(score):
    """ Return a grade based on a given connection score """
    grade = {1: 'A', 2: 'A-', 3: 'B', 4: 'B-', 5: 'C',
             6: 'C-', 7: 'F', 8: 'F-', 9: 'T', 10: '?'}
    return grade[score] if score in grade else '?'


def grab_xmpp():
    """ Grab the table data from XMPP.net
        Sort by Client-Server, Server-Server, then by Name of XMPP
        Returns a PrettyTable of sorted XMPP servers """

    r = get('https://xmpp.net/directory.php')
    soup = BeautifulSoup(r.text)
    rows = list()
    for each in soup.find_all('tr'):
        row = list()
        for link in each.find_all('a'):
            url = link.get('href')
            if url.startswith('http'):
                row.append(url)
                break
        for b in each.find_all('td'):
            if b is not None:
                row.append(b.text.strip())
        if row:
            rows.append((row[1], g2s(row[7]), g2s(row[8]), row[3], row[0]))

    rows.sort(key=itemgetter(1, 2, 0))  # C-S, S-S, Server Name
    xmpp_table = PrettyTable(['Name', 'C-S', 'S-S', 'Country', 'Url'])

    for name, c2s, s2s, country, url in rows:
        xmpp_table.add_row([name, s2g(c2s), s2g(s2s), country, url])

    return xmpp_table

if __name__ == '__main__':
    print(grab_xmpp())
