# https://en.wikipedia.org/wiki/Conversion_of_units_of_temperature


def celsius_to_delisle(c):
    return (100 - c) * (3 / 2.0)


def celsius_to_fahrenheit(c):
    return c * (9 / 5.0) + 32


def celsius_to_kelvin(c):
    return c + 273.15


def celsius_to_newton(c):
    return c * (33 / 100.0)


def celsius_to_rankine(c):
    return (c + 273.15) * (9 / 5.0)


def celsius_to_reaumur(c):
    return c * (4 / 5.0)


def celsius_to_romer(c):
    return c * (21 / 40.0) + 7.5


def delisle_to_celsius(de):
    return 100 - de * (2 / 3.0)


def delisle_to_fahrenheit(de):
    return 212 - de * (6 / 5.0)


def delisle_to_kelvin(de):
    return 373.15 - de * (2 / 3.0)


def delisle_to_newton(de):
    return 33 - de * (11 / 50.0)


def delisle_to_rankine(de):
    return 671.67 - de * (6 / 5.0)


def delisle_to_reaumur(de):
    return 80 - de * (8 / 15.0)


def delisle_to_romer(de):
    return 60 - de * (7 / 20.0)


def fahrenheit_to_celsius(f):
    return (f - 32) * (5 / 9.0)


def fahrenheit_to_delisle(f):
    return (212 - f) * (5 / 6.0)


def fahrenheit_to_kelvin(f):
    return (f + 459.67) * (5 / 9.0)


def fahrenheit_to_newton(f):
    return (f - 32) * (11 / 60.0)


def fahrenheit_to_rankine(f):
    return f + 459.67


def fahrenheit_to_reaumur(f):
    return (f - 32) * (4 / 9.0)


def fahrenheit_to_romer(f):
    return (f - 32) * (7 / 24.0) + 7.5


def kelvin_to_celsius(k):
    return k - 273.15


def kelvin_to_delisle(k):
    return (373.15 - k) * (3 / 2.0)


def kelvin_to_fahrenheit(k):
    return k * (9 / 5.0) - 459.67


def kelvin_to_newton(k):
    return (k - 273.15) * (33 / 100.0)


def kelvin_to_rankine(k):
    return k * (9 / 5.0)


def kelvin_to_reaumur(k):
    return (k - 273.15) * (4 / 5.0)


def kelvin_to_romer(k):
    return (k - 273.15) * (21 / 40.0) + 7.5


def newton_to_celsius(n):
    return n * (100 / 33.0)


def newton_to_delisle(n):
    return (33 - n) * (50 / 11.0)


def newton_to_fahrenheit(n):
    return n * (60 / 11.0) + 32


def newton_to_kelvin(n):
    return n * (100 / 33.0) + 273.15


def newton_to_rankine(n):
    return n * (60 / 11.0) + 491.67


def newton_to_reaumur(n):
    return n * (80 / 33.0)


def newton_to_romer(n):
    return n * (35 / 22.0) + 7.5


def rankine_to_celsius(r):
    return (r - 491.67) * (5 / 9.0)


def rankine_to_delisle(r):
    return (671.67 - r) * (5 / 6.0)


def rankine_to_fahrenheit(r):
    return r - 459.67


def rankine_to_kelvin(r):
    return r * (5 / 9.0)


def rankine_to_newton(r):
    return (r - 491.67) * (11 / 60.0)


def rankine_to_reaumur(r):
    return (r - 491.67) * (4 / 9.0)


def rankine_to_romer(r):
    return (r - 491.67) * (7 / 24.0) + 7.5


def reaumur_to_celsius(re):
    return re * (5 / 4.0)


def reaumur_to_delisle(re):
    return (80 - re) * (15 / 8.0)


def reaumur_to_fahrenheit(re):
    return re * (9 / 4.0) + 32


def reaumur_to_kelvin(re):
    return re * (5 / 4.0) + 273.15


def reaumur_to_newton(re):
    return re * (33 / 80.0)


def reaumur_to_rankine(re):
    return re * (9 / 4.0) + 491.67


def reaumur_to_romer(re):
    return re * (21 / 32.0) + 7.5


def romer_to_celsius(ro):
    return (ro - 7.5) * (40 / 21.0)


def romer_to_delisle(ro):
    return (60 - ro) * (20 / 7.0)


def romer_to_fahrenheit(ro):
    return (ro - 7.5) * (24 / 7.0) + 32


def romer_to_kelvin(ro):
    return (ro - 7.5) * (40 / 21.0) + 273.15


def romer_to_newton(ro):
    return (ro - 7.5) * (22 / 35.0)


def romer_to_rankine(ro):
    return (ro - 7.5) * (24 / 7.0) + 491.67


def romer_to_reaumur(ro):
    return (ro - 7.5) * (32 / 21.0)
