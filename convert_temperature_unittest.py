import convert_temperature as temp
import unittest


class TemperatureTests(unittest.TestCase):
    def test_c_d(self):
        self.assertEqual(round(temp.celsius_to_delisle(300), 2), -300.00)

    def test_c_f(self):
        self.assertEqual(round(temp.celsius_to_fahrenheit(300), 2), 572.00)

    def test_c_k(self):
        self.assertEqual(round(temp.celsius_to_kelvin(300), 2), 573.15)

    def test_c_n(self):
        self.assertEqual(round(temp.celsius_to_newton(300), 2), 99.00)

    def test_c_r(self):
        self.assertEqual(round(temp.celsius_to_rankine(300), 2), 1031.67)

    def test_c_re(self):
        self.assertEqual(round(temp.celsius_to_reaumur(300), 2), 240.00)

    def test_c_ro(self):
        self.assertEqual(round(temp.celsius_to_romer(300), 2), 165.00)

    def test_d_c(self):
        self.assertEqual(round(temp.delisle_to_celsius(-225), 2), 250.00)

    def test_d_f(self):
        self.assertEqual(round(temp.delisle_to_fahrenheit(-225), 2), 482.00)

    def test_d_k(self):
        self.assertEqual(round(temp.delisle_to_kelvin(-225), 2), 523.15)

    def test_d_n(self):
        self.assertEqual(round(temp.delisle_to_newton(-225), 2), 82.50)

    def test_d_r(self):
        self.assertEqual(round(temp.delisle_to_rankine(-225), 2), 941.67)

    def test_d_re(self):
        self.assertEqual(round(temp.delisle_to_reaumur(-225), 2), 200.00)

    def test_d_ro(self):
        self.assertEqual(round(temp.delisle_to_romer(-225), 2), 138.75)

    def test_f_c(self):
        self.assertEqual(round(temp.fahrenheit_to_celsius(374.00), 2), 190.00)

    def test_f_d(self):
        self.assertEqual(round(temp.fahrenheit_to_delisle(374.00), 2), -135.00)

    def test_f_k(self):
        self.assertEqual(round(temp.fahrenheit_to_kelvin(374.00), 2), 463.15)

    def test_f_n(self):
        self.assertEqual(round(temp.fahrenheit_to_newton(374.00), 2), 62.70)

    def test_f_r(self):
        self.assertEqual(round(temp.fahrenheit_to_rankine(374.00), 2), 833.67)

    def test_f_re(self):
        self.assertEqual(round(temp.fahrenheit_to_reaumur(374.00), 2), 152.00)

    def test_f_ro(self):
        self.assertEqual(round(temp.fahrenheit_to_romer(374.00), 2), 107.25)

    def test_k_c(self):
        self.assertEqual(round(temp.kelvin_to_celsius(383.15), 2), 110.00)

    def test_k_d(self):
        self.assertEqual(round(temp.kelvin_to_delisle(383.15), 2), -15.00)

    def test_k_f(self):
        self.assertEqual(round(temp.kelvin_to_fahrenheit(383.15), 2), 230.00)

    def test_k_n(self):
        self.assertEqual(round(temp.kelvin_to_newton(383.15), 2), 36.30)

    def test_k_r(self):
        self.assertEqual(round(temp.kelvin_to_rankine(383.15), 2), 689.67)

    def test_k_re(self):
        self.assertEqual(round(temp.kelvin_to_reaumur(383.15), 2), 88.00)

    def test_k_ro(self):
        self.assertEqual(round(temp.kelvin_to_romer(383.15), 2), 65.25)

    def test_n_c(self):
        self.assertEqual(round(temp.newton_to_celsius(9.90), 2), 30.00)

    def test_n_d(self):
        self.assertEqual(round(temp.newton_to_delisle(9.90), 2), 105.00)

    def test_n_f(self):
        self.assertEqual(round(temp.newton_to_fahrenheit(9.90), 2), 86.00)

    def test_n_k(self):
        self.assertEqual(round(temp.newton_to_kelvin(9.90), 2), 303.15)

    def test_n_r(self):
        self.assertEqual(round(temp.newton_to_rankine(9.90), 2), 545.67)

    def test_n_re(self):
        self.assertEqual(round(temp.newton_to_reaumur(9.90), 2), 24.00)

    def test_n_ro(self):
        self.assertEqual(round(temp.newton_to_romer(9.90), 2), 23.25)

    def test_r_c(self):
        self.assertEqual(round(temp.rankine_to_celsius(455.67), 2), -20.00)

    def test_r_d(self):
        self.assertEqual(round(temp.rankine_to_delisle(455.67), 2), 180.00)

    def test_r_f(self):
        self.assertEqual(round(temp.rankine_to_fahrenheit(455.67), 2), -4.00)

    def test_r_k(self):
        self.assertEqual(round(temp.rankine_to_kelvin(455.67), 2), 253.15)

    def test_r_n(self):
        self.assertEqual(round(temp.rankine_to_newton(455.67), 2), -6.60)

    def test_r_re(self):
        self.assertEqual(round(temp.rankine_to_reaumur(455.67), 2), -16.00)

    def test_r_ro(self):
        self.assertEqual(round(temp.rankine_to_romer(455.67), 2), -3.00)

    def test_re_c(self):
        self.assertEqual(round(temp.reaumur_to_celsius(-88.00), 2), -110.00)

    def test_re_d(self):
        self.assertEqual(round(temp.reaumur_to_delisle(-88.00), 2), 315.00)

    def test_re_f(self):
        self.assertEqual(round(temp.reaumur_to_fahrenheit(-88.00), 2), -166.00)

    def test_re_k(self):
        self.assertEqual(round(temp.reaumur_to_kelvin(-88.00), 2), 163.15)

    def test_re_n(self):
        self.assertEqual(round(temp.reaumur_to_newton(-88.00), 2), -36.30)

    def test_re_r(self):
        self.assertEqual(round(temp.reaumur_to_rankine(-88.00), 2), 293.67)

    def test_re_ro(self):
        self.assertEqual(round(temp.reaumur_to_romer(-88.00), 2), -50.25)

    def test_ro_c(self):
        self.assertEqual(round(temp.romer_to_celsius(23.25), 2), 30.00)

    def test_ro_d(self):
        self.assertEqual(round(temp.romer_to_delisle(23.25), 2), 105.00)

    def test_ro_f(self):
        self.assertEqual(round(temp.romer_to_fahrenheit(23.25), 2), 86.00)

    def test_ro_k(self):
        self.assertEqual(round(temp.romer_to_kelvin(23.25), 2), 303.15)

    def test_ro_n(self):
        self.assertEqual(round(temp.romer_to_newton(23.25), 2), 9.90)

    def test_ro_r(self):
        self.assertEqual(round(temp.romer_to_rankine(23.25), 2), 545.67)

    def test_ro_re(self):
        self.assertEqual(round(temp.romer_to_reaumur(23.25), 2), 24.00)

if __name__ == '__main__':
    unittest.main()
