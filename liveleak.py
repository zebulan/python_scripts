from bs4 import BeautifulSoup, SoupStrainer
from os import path
from requests import get
from time import time, strptime


def scrape(url):
    r = get(url)
    soup = BeautifulSoup(r.text, "xml", parse_only=SoupStrainer('item'))
    for x in soup:
        old_t = x.pubDate.text.split()
        new_t = strptime(' '.join(old_t[:-2]), "%a, %d %b %Y")
        new_t = '/'.join((str(new_t.tm_year), str(new_t.tm_mon),
                          str(new_t.tm_mday), old_t[-2]))
        title = ' '.join((x.title.text.split()))
        entry = '***'.join((new_t, x.link.text, title)) + '\n'
        if entry not in results:
            tmp.append(entry)

if __name__ == '__main__':
    tmp = []  # deque for threading?
    results = set()  # in case 'results.csv' doesn't exist
    if path.exists('liveleak.csv'):
        with open('liveleak.csv', 'r') as f:
            results = f.readlines()
        results = set(results)  # if I don't do this, must do it for union
    start = time()
    # scrape(leak+'1')
    leak = 'http://www.liveleak.com/rss?featured=1&page='
    [scrape(leak+str(x)) for x in range(418)]
    if tmp:
        total = set(results.union(tmp))
        with open('liveleak.csv', 'w') as f:
            [f.write(x) for x in sorted(total, reverse=True) if x]
    print('Elapsed Time:', time() - start)
