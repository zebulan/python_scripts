from bs4 import BeautifulSoup
from requests import get
from time import time
from unicodedata import normalize


def bytestring_to_string(txt):
    return normalize('NFKD', txt).encode('ascii', 'ignore').decode('utf-8')

with open('documentaries.csv', 'w') as doc:
    page_number = 1
    start = time()
    url = 'http://thoughtmaybe.com/browse/page/'
    while True:
        r = get(''.join((url, str(page_number))))
        if not r.status_code == 200:  # end of list (currently 18 pages)
            break
        soup = BeautifulSoup(r.text)
        for video in soup.find_all('div', class_='col-md-3 col-sm-6 col-xs-6 '):
            row = list()
            row.append(video.h3.a.text)
            row.append(video.find('span', class_='item-author').text)
            try:
                row.append(video.find('span', class_='item-date').text)
            except AttributeError:
                row.append('Unknown')
            row.append(video.h3.a['href'])
            row.append(video.find('div', class_='item-content hidden').text)

            # title *** author *** date *** link *** description
            doc.write(bytestring_to_string('***'.join(row)))
        page_number += 1
    print('Elapsed Time: {:.3f} seconds.'.format(time() - start))
